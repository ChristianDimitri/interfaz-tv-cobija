﻿using System.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Data.SqlClient;
using MinimalisticTelnet;
using System.Text.RegularExpressions;
using SnmpSharpNet;
using System.Net;
using InterfazMikrotik;

namespace InterfazTelnet
{
    class Program
    {

        public System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
        public List<OLT> listaOLT;
        public List<ComandoProcesado> comandosProcesados = new List<ComandoProcesado>();
        public bool procesando = false; 

        /// <summary>
        /// Función Principal para el Inicio de la Interfaz
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            Program obj = new Program();
            Console.ReadLine();
        }

        /// <summary>
        /// Constructor, inicializa el timer y obtiene las OLT almacenadas en la tabla de GENERALS para el acceso por Telnet
        /// </summary>
        public Program()
        {
            while (true)
            {
                ProcesaCNR();
                System.Threading.Thread.Sleep(15000);
            }
        }

        private void TimerCallback(Object o)
        {
            ProcesaCNR();
        }

        /// <summary>
        /// Funcion principal, aqui se inicia la busqueda de comandos, obtiene la ip para la conexion, y ejecuta los comandos qe se tengan pendientes
        /// </summary>
        private void ProcesaCNR()
        {
            Console.WriteLine("Buscando comandos...");

            List<Comandos> comandos = new List<Comandos>();
            DBFuncion dbComandos = new DBFuncion();
            SqlDataReader reader = dbComandos.consultaReader("PROCESATvTelnet");
            comandos = dbComandos.MapDataToEntityCollection<Comandos>(reader).ToList();

            string ip = "";

            foreach (Comandos comandoAux in comandos)
            {
                ip = "";
                ip = getIpByMacPPoE(comandoAux.MacWan);

                Console.WriteLine(comandoAux.comando + "\t" + ip + "\t" + comandoAux.MacLan);
                if (!string.IsNullOrEmpty(ip))//Revisa primero si tenemos la ip para hacer la conexión
                {
                    switch(comandoAux.comando){//busca el comando
                        case "activar":
                            Activar(ip, comandoAux.TipoOnu, comandoAux.Consecutivo);
                            break;
                        case "suspender":
                            Suspender(ip, comandoAux.TipoOnu, comandoAux.Consecutivo);
                            break;
                        case "borrar":
                            Borrar(ip, comandoAux.TipoOnu, comandoAux.Consecutivo);
                            break;
                        case "reactivar":
                            Reactivar(ip, comandoAux.TipoOnu, comandoAux.Consecutivo);
                            break;
                        case "activarsolotv":
                            ActivarSoloTv(ip, comandoAux.TipoOnu, comandoAux.Consecutivo);
                            break;
                        case "borrarsolotv":
                            BorrarSoloTv(ip, comandoAux.TipoOnu, comandoAux.Consecutivo);
                            break;
                        default:
                            Console.WriteLine("No se pudo obtener el comando");
                            DBFuncion db2 = new DBFuncion();
                            db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 2);
                            db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, comandoAux.Consecutivo);
                            db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "No se pudo obtener el comando");
                            db2.consultaSinRetorno("ejecutaTvTelnet");
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("No se obtuvo una IP para la conexión");
                    DBFuncion db3 = new DBFuncion();
                    db3.agregarParametro("@resultado", System.Data.SqlDbType.Int, 30);
                    db3.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, comandoAux.Consecutivo);
                    db3.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "No se obtuvo una IP para la conexión");
                    db3.consultaSinRetorno("ejecutaTvTelnet");
                }

            }
            reader.Close();
            dbComandos.conexion.Close();
        }

        /// <summary>
        /// Procesa comandos de Activacion dependiendo del tipo de onu que recibe
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="tipoonu"></param>
        /// <param name="consecutivo"></param>
        public void Activar(string ip,int tipoonu, long consecutivo)
        {
            try
            {
                if (tipoonu == 1){
                    Console.WriteLine("Conectando...");
                    TelnetConnection tc1 = new TelnetConnection(ip, 23);
                    string s1 = tc1.Login("root", "root626", 5000);
                    Console.WriteLine(ip);
                    Console.WriteLine(s1);
                    sendCommand(tc1, "flash set CATV_ENABLED 1");
                    sendCommand(tc1, "reboot");
                    tc1.CloseConnection();

                    Console.WriteLine(consecutivo + "\t" + "1" + "\t" + "Activacion completada");
                    DBFuncion db2 = new DBFuncion();
                    db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 1);
                    db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int,consecutivo);
                    db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "Activacion completada");
                    db2.consultaSinRetorno("ejecutaTvTelnet");
                }
                else if (tipoonu == 2){
                    Console.WriteLine("Conectando...");
                    TelnetConnection tc1 = new TelnetConnection(ip, 23);
                    string s1 = tc1.Login("super", "superxlp", 5000);
                    Console.WriteLine(ip);
                    Console.WriteLine(s1);
                    sendCommand(tc1, "enable");
                    sendCommand(tc1, "/system/catv/set switch allon");
                    tc1.CloseConnection();

                    Console.WriteLine(consecutivo + "\t" + "1" + "\t" + "Activacion completada");
                    DBFuncion db2 = new DBFuncion();
                    db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 1);
                    db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int,consecutivo);
                    db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "Activacion completada");
                    db2.consultaSinRetorno("ejecutaTvTelnet");
                }
                else{
                    Console.WriteLine(consecutivo + "\t" + "2" + "\t" + "Tipo onu = 0");
                    DBFuncion db2 = new DBFuncion();
                    db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 2);
                    db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int,consecutivo);
                    db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "Onu Tipo 0");
                    db2.consultaSinRetorno("ejecutaTvTelnet");
                }
            }catch(Exception ex){
                Console.WriteLine("Exception activacion: " + ex.Message);
                DBFuncion db2 = new DBFuncion();
                db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 2);
                db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int,consecutivo);
                db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "Exception activacion: "+ ex.Message);
                db2.consultaSinRetorno("ejecutaTvTelnet");
            }
        }

        /// <summary>
        /// Procesa comandos de Suspender dependiendo del tipo de onu que recibe
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="tipoonu"></param>
        /// <param name="consecutivo"></param>
        public void Suspender(string ip, int tipoonu, long consecutivo)
        {
            try
            {
                if (tipoonu == 1)
                {
                    Console.WriteLine("Conectando...");
                    TelnetConnection tc2 = new TelnetConnection(ip, 23);
                    string s2 = tc2.Login("root", "root626", 5000);
                    Console.WriteLine(ip);
                    Console.WriteLine(s2);
                    sendCommand(tc2, "flash set CATV_ENABLED 0");
                    sendCommand(tc2, "reboot");
                    tc2.CloseConnection();

                    Console.WriteLine(consecutivo + "\t" + "1" + "\t" + "Suspender completada");
                    DBFuncion db2 = new DBFuncion();
                    db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 1);
                    db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, consecutivo);
                    db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "Suspender completada");
                    db2.consultaSinRetorno("ejecutaTvTelnet");
                }
                else if (tipoonu == 2)
                {
                    Console.WriteLine("Conectando...");
                    TelnetConnection tc2 = new TelnetConnection(ip, 23);
                    string s2 = tc2.Login("super", "superxlp", 5000);
                    Console.WriteLine(ip);
                    Console.WriteLine(s2);
                    sendCommand(tc2, "enable");
                    sendCommand(tc2, "/system/catv/set switch alloff");
                    tc2.CloseConnection();

                    Console.WriteLine(consecutivo + "\t" + "1" + "\t" + "Suspender completada");
                    DBFuncion db2 = new DBFuncion();
                    db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 1);
                    db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, consecutivo);
                    db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "Suspender completada");
                    db2.consultaSinRetorno("ejecutaTvTelnet");
                }
                else
                {
                    Console.WriteLine(consecutivo + "\t" + "2" + "\t" + "Tipo onu = 0");
                    DBFuncion db2 = new DBFuncion();
                    db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 2);
                    db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, consecutivo);
                    db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "Onu Tipo 0");
                    db2.consultaSinRetorno("ejecutaTvTelnet");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception suspender: " + ex.Message);
                DBFuncion db2 = new DBFuncion();
                db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 2);
                db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, consecutivo);
                db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "Exception suspender: " + ex.Message);
                db2.consultaSinRetorno("ejecutaTvTelnet");
            }
        }

        /// <summary>
        /// Procesa comandos de Borrar dependiendo del tipo de onu que recibe
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="tipoonu"></param>
        /// <param name="consecutivo"></param>
        public void Borrar(string ip, int tipoonu, long consecutivo)
        {
            try
            {
                if (tipoonu == 1)
                {
                    Console.WriteLine("Conectando...");
                    TelnetConnection tc2 = new TelnetConnection(ip, 23);
                    string s2 = tc2.Login("root", "root626", 5000);
                    Console.WriteLine(ip);
                    Console.WriteLine(s2);
                    sendCommand(tc2, "flash set CATV_ENABLED 0");
                    sendCommand(tc2, "reboot");
                    tc2.CloseConnection();

                    Console.WriteLine(consecutivo + "\t" + "1" + "\t" + "Borrar completada");
                    DBFuncion db2 = new DBFuncion();
                    db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 1);
                    db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, consecutivo);
                    db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "Borrar completada");
                    db2.consultaSinRetorno("ejecutaTvTelnet");
                }
                else if (tipoonu == 2)
                {
                    Console.WriteLine("Conectando...");
                    TelnetConnection tc2 = new TelnetConnection(ip, 23);
                    string s2 = tc2.Login("super", "superxlp", 5000);
                    Console.WriteLine(ip);
                    Console.WriteLine(s2);
                    sendCommand(tc2, "enable");
                    sendCommand(tc2, "/system/catv/set switch alloff");
                    tc2.CloseConnection();

                    Console.WriteLine(consecutivo + "\t" + "1" + "\t" + "Borrar completada");
                    DBFuncion db2 = new DBFuncion();
                    db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 1);
                    db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, consecutivo);
                    db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "Borrar completada");
                    db2.consultaSinRetorno("ejecutaTvTelnet");
                }
                else
                {
                    Console.WriteLine(consecutivo + "\t" + "2" + "\t" + "Tipo onu = 0");
                    DBFuncion db2 = new DBFuncion();
                    db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 2);
                    db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, consecutivo);
                    db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "Onu Tipo 0");
                    db2.consultaSinRetorno("ejecutaTvTelnet");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception borrar: " + ex.Message);
                DBFuncion db2 = new DBFuncion();
                db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 2);
                db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, consecutivo);
                db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "Exception borrar: " + ex.Message);
                db2.consultaSinRetorno("ejecutaTvTelnet");
            }
        }

        /// <summary>
        /// Procesa coamndos de Reactivacion dependiendo del tipo de onu que recibe
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="tipoonu"></param>
        /// <param name="consecutivo"></param>
        public void Reactivar(string ip, int tipoonu, long consecutivo)
        {
            try
            {
                if (tipoonu == 1)
                {
                    Console.WriteLine("Conectando...");
                    TelnetConnection tc1 = new TelnetConnection(ip, 23);
                    string s1 = tc1.Login("root", "root626", 5000);
                    Console.WriteLine(ip);
                    Console.WriteLine(s1);
                    sendCommand(tc1, "flash set CATV_ENABLED 1");
                    sendCommand(tc1, "reboot");
                    tc1.CloseConnection();

                    Console.WriteLine(consecutivo + "\t" + "1" + "\t" + "Reactivacion completada");
                    DBFuncion db2 = new DBFuncion();
                    db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 1);
                    db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, consecutivo);
                    db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "Reactivacion completada");
                    db2.consultaSinRetorno("ejecutaTvTelnet");
                }
                else if (tipoonu == 2)
                {
                    Console.WriteLine("Conectando...");
                    TelnetConnection tc1 = new TelnetConnection(ip, 23);
                    string s1 = tc1.Login("super", "superxlp", 5000);
                    Console.WriteLine(ip);
                    Console.WriteLine(s1);
                    sendCommand(tc1, "enable");
                    sendCommand(tc1, "/system/catv/set switch allon");
                    tc1.CloseConnection();

                    Console.WriteLine(consecutivo + "\t" + "1" + "\t" + "Reactivar completada");
                    DBFuncion db2 = new DBFuncion();
                    db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 1);
                    db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, consecutivo);
                    db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "Reactivar completada");
                    db2.consultaSinRetorno("ejecutaTvTelnet");
                }
                else
                {
                    Console.WriteLine(consecutivo + "\t" + "2" + "\t" + "Tipo onu = 0");
                    DBFuncion db2 = new DBFuncion();
                    db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 2);
                    db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, consecutivo);
                    db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "Onu Tipo 0");
                    db2.consultaSinRetorno("ejecutaTvTelnet");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception reactivacion: " + ex.Message);
                DBFuncion db2 = new DBFuncion();
                db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 2);
                db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, consecutivo);
                db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "Exception reactivacion: " + ex.Message);
                db2.consultaSinRetorno("ejecutaTvTelnet");
            }
        }
        
        /// <summary>
        /// Procesa comandos de Activacion solo tv dependiendo del tipo de onu que recibe
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="tipoonu"></param>
        /// <param name="consecutivo"></param>
        public void ActivarSoloTv(string ip, int tipoonu, long consecutivo)
        {
            try
            {
                if (tipoonu == 1)
                {
                    Console.WriteLine("Conectando...");
                    TelnetConnection tc1 = new TelnetConnection(ip, 23);
                    string s1 = tc1.Login("root", "root626", 5000);
                    Console.WriteLine(ip);
                    Console.WriteLine(s1);
                    sendCommand(tc1, "flash set CATV_ENABLED 1");
                    sendCommand(tc1, "reboot");
                    tc1.CloseConnection();

                    Console.WriteLine(consecutivo + "\t" + "1" + "\t" + "Activacion Solo TV completada");
                    DBFuncion db2 = new DBFuncion();
                    db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 1);
                    db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, consecutivo);
                    db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "Activacion Solo TV completada");
                    db2.consultaSinRetorno("ejecutaTvTelnet");
                }
                else if (tipoonu == 2)
                {
                    Console.WriteLine("Conectando...");
                    TelnetConnection tc1 = new TelnetConnection(ip, 23);
                    string s1 = tc1.Login("super", "superxlp", 5000);
                    Console.WriteLine(ip);
                    Console.WriteLine(s1);
                    sendCommand(tc1, "enable");
                    sendCommand(tc1, "/system/catv/set switch allon");
                    tc1.CloseConnection();

                    Console.WriteLine(consecutivo + "\t" + "1" + "\t" + "Activacion Solo TV completada");
                    DBFuncion db2 = new DBFuncion();
                    db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 1);
                    db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, consecutivo);
                    db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "Activacion Solo TV completada");
                    db2.consultaSinRetorno("ejecutaTvTelnet");
                }
                else
                {
                    Console.WriteLine(consecutivo + "\t" + "2" + "\t" + "Tipo onu = 0");
                    DBFuncion db2 = new DBFuncion();
                    db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 2);
                    db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, consecutivo);
                    db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "Onu Tipo 0");
                    db2.consultaSinRetorno("ejecutaTvTelnet");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception activarsolotv: " + ex.Message);
                DBFuncion db2 = new DBFuncion();
                db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 2);
                db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, consecutivo);
                db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "Exception activarsolotv: " + ex.Message);
                db2.consultaSinRetorno("ejecutaTvTelnet");
            }
        }

        /// <summary>
        /// Procesa comandos de Borrar solo tv dependiendo del tipo de onu que recibe
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="tipoonu"></param>
        /// <param name="consecutivo"></param>
        public void BorrarSoloTv(string ip, int tipoonu, long consecutivo)
        {
            try
            {
                if (tipoonu == 1)
                {
                    Console.WriteLine("Conectando...");
                    TelnetConnection tc2 = new TelnetConnection(ip, 23);
                    string s2 = tc2.Login("root", "root626", 5000);
                    Console.WriteLine(ip);
                    Console.WriteLine(s2);
                    sendCommand(tc2, "flash set CATV_ENABLED 0");
                    sendCommand(tc2, "reboot");
                    tc2.CloseConnection();

                    Console.WriteLine(consecutivo + "\t" + "1" + "\t" + "Borrar Solo TV completada");
                    DBFuncion db2 = new DBFuncion();
                    db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 10);
                    db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, consecutivo);
                    db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "Borrar Solo TV completada");
                    db2.consultaSinRetorno("ejecutaTvTelnet");
                }
                else if (tipoonu == 2)
                {
                    Console.WriteLine("Conectando...");
                    TelnetConnection tc2 = new TelnetConnection(ip, 23);
                    string s2 = tc2.Login("super", "superxlp", 5000);
                    Console.WriteLine(ip);
                    Console.WriteLine(s2);
                    sendCommand(tc2, "enable");
                    sendCommand(tc2, "/system/catv/set switch alloff");
                    tc2.CloseConnection();

                    Console.WriteLine(consecutivo + "\t" + "1" + "\t" + "Borrar Solo TV completada");
                    DBFuncion db2 = new DBFuncion();
                    db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 10);
                    db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, consecutivo);
                    db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "Borrar Solo TV completada");
                    db2.consultaSinRetorno("ejecutaTvTelnet");
                }
                else
                {
                    Console.WriteLine(consecutivo + "\t" + "2" + "\t" + "Tipo onu = 0");
                    DBFuncion db2 = new DBFuncion();
                    db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 2);
                    db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, consecutivo);
                    db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "Onu Tipo 0");
                    db2.consultaSinRetorno("ejecutaTvTelnet");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception borrarsolotv: " + ex.Message);
                DBFuncion db2 = new DBFuncion();
                db2.agregarParametro("@resultado", System.Data.SqlDbType.Int, 2);
                db2.agregarParametro("@consecutivo", System.Data.SqlDbType.Int, consecutivo);
                db2.agregarParametro("@mensaje", System.Data.SqlDbType.VarChar, "Exception borrarsolotv: " + ex.Message);
                db2.consultaSinRetorno("ejecutaTvTelnet");
            }
        }


        public static String getIpByMacPPoE(String Name)
        {
	        MK mikrotik;
	        mikrotik = new MK("45.226.35.2"); //new MK("192.168.50.201");
            if (!mikrotik.Login("SOFTV", "Softv*123")) //if (!mikrotik.Login("admin", ""))
            {
                Console.WriteLine("Could not log in");
                mikrotik.Close();
                Console.ReadLine();
                return "";
            }
            Boolean flag = false;
            String Respuesta = "";
            mikrotik.Send("/ppp/active/print");
            mikrotik.Send("=.proplist=address");
            mikrotik.Send("?name=" + Name, true);

            foreach (string h in mikrotik.Read())
            {
                if (h.Contains("address"))
                {
                    flag = true;
                    Respuesta = h.Substring(12);
                    Console.WriteLine(Respuesta);
                }
                else if (flag == false)
                {
                    Respuesta = "";
                    Console.WriteLine(Respuesta);
                }
                Console.WriteLine(h);

            }
            mikrotik.Close();
            return Respuesta;
        }

        /// <summary>
        /// Verifica que se obtenga el prompt correspondiente si la conexión por Telnet fue exitosa
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public bool getConnectionSuccess(string s)
        {
            string prompt = s.TrimEnd();
            prompt = s.Substring(prompt.Length - 1, 1);
            if (prompt != "$" && prompt != ">" && prompt != "#")
                return false;
            return true;
        }

        /// <summary>
        /// Envía el comando correspondiente por Telnet y obtiene la salida del comando línea por línea
        /// </summary>
        /// <param name="tc"></param>
        /// <param name="command"></param>
        /// <returns></returns>
        public String[] sendCommand(TelnetConnection tc, string command)
        {
            if (tc.IsConnected)
            {
                tc.WriteLine(command);
                System.Threading.Thread.Sleep(5000);
                string prompt = tc.Read();

                Console.WriteLine(command);
                Console.WriteLine(prompt);

                if (prompt.Contains("Press any key to continue"))
                {
                    string promptAux = "";
                    do
                    {
                        promptAux = "";
                        tc.WriteLine("S");
                        System.Threading.Thread.Sleep(5000);

                        promptAux = tc.Read();
                        prompt = prompt + promptAux;
                    } while (promptAux.Contains("Press any key to continue"));
                }

                String[] strlist = prompt.Split('\n');

                return strlist;
            }
            else
            {
                String[] aux = { "Disconnected..." };
                return aux;
            }
        }

    }//end class
}//end namespace
