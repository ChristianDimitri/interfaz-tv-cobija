﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfazTelnet
{
    /// <summary>
    /// Clase de Manejo de la información de las OLT almacenadas en la tabla GENERALS de la base de datos
    /// </summary>
    public class OLT
    {
        public int Id { get; set; }
        public string Ip { get; set; }
        public string User_ { get; set; }
        public string Pass { get; set; }
    }

    public class Comandos
    {
        public long Consecutivo { get; set; }
        public string Contrato { get; set; }
        public string MacLan { get; set; }
        public string MacWan { get; set; }
        public string paquete { get; set; }
        public string ip { get; set; }
        public string comando { get; set; }
        public int TipoOnu { get; set; }
        public string NombreOnu { get; set; }
    }

    public class ComandoProcesado
    {
        public long Consecutivo { get; set; }
        public bool Procesado { get; set; }
    }
}
